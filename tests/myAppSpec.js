describe("First Test", function () {

    var mockScope = {};
    var controller;

    beforeEach(angular.mock.module("demoApp"));

    beforeEach(angular.mock.inject(function ($controller, $rootScope) {
        mockScope = $rootScope.$new();
        localStorage.clear();
        controller = $controller("MainController", {
            $scope: mockScope
        });
    }));

    it("Processes the data", function () {
        expect(mockScope.vm.elements).toBeDefined();
        var length = Object.keys(mockScope.vm.elements).length;
        expect(length).toEqual(3);
    });

    it("Add to root scope element", function () {
        mockScope.vm.incrementCounter('1');
        var target = mockScope.vm.elements['1'];
        var length = Object.keys(target.sub).length;
        expect(length).toEqual(1);
    });

});