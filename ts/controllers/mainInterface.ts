interface IMainController{
    elements: Object;
    incrementCounter: (name:string, obj?:Object) => void;
    getElements: (name: string) => Object;
}