/// <reference path='mainInterface.ts' />
/// <reference path='../modules.ts' />

class MainController implements IMainController{


    static $inject = ['$scope', 'dataStore'];

    public elements;

    private reversIndex: number = 0;

    getElements(name):Object {
        let target: Object = this.elements[name];
        if (Object.keys(target).length > 0) return target
        else return null;
    }
    
    constructor($scope, private dataModel:dataStore){
       $scope.vm = this;
       this.elements = dataModel.getFromLocalstorage();
    }

    incrementCounter(elm, obj):void{
        var allElments: Array<any> = elm.split('_');
        var keyEl: string = allElments[0];
        if (this.reversIndex > 0) for (var a: number = 0; a <= this.reversIndex; a++) keyEl += allElments[a];
        var currentTarget = (obj) ? obj[elm] : this.elements[keyEl];
        
        var propCount: number = Object.keys(currentTarget['sub']).length;
        
        if (propCount > 0 && allElments[this.reversIndex + 1]) {
            this.reversIndex++;
            this.incrementCounter(elm, currentTarget['sub']);
        }
        else {
            this.reversIndex = 0;  
            let childEl: Object = { 'sub': {} };
            let keyString: string = (propCount == 0) ? (elm + '_1') : (elm + '_' + (propCount + 1));  
            currentTarget['sub'][keyString] = childEl;
            this.dataModel.saveToLocalstorage(this.elements);
        }

    }
}

controllers.controller('MainController', MainController);