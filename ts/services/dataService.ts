/// <reference path='../modules.ts' />

class dataStore {
    static $inject: Array<string> = ['$rootScope'];

    private elements:Object = {}

    constructor($rootScope){

    }

    public getFromLocalstorage():Object{
		var localData: any = localStorage.getItem("demo");
		if (!localData) {
			localData = {
				'1': {
					sub:{}
				},
				'2': {
					sub:{}
				},
				'3': {
					sub:{
					}
				}
			}

			this.saveToLocalstorage(localData);
		}
		else localData = JSON.parse(localData);
		return localData;
    }

    public  saveToLocalstorage(data: any):void{
		localStorage.setItem("demo", JSON.stringify(data));
    }
}

services.service('dataStore', dataStore);