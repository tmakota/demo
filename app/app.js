/// <reference path='refference.ts' />
var services = angular.module('services', []);
var controllers = angular.module('controllers', []);
/// <reference path='../modules.ts' />
var dataStore = (function () {
    function dataStore($rootScope) {
        this.elements = {};
    }
    dataStore.prototype.getFromLocalstorage = function () {
        var localData = localStorage.getItem("demo");
        if (!localData) {
            localData = {
                '1': {
                    sub: {}
                },
                '2': {
                    sub: {}
                },
                '3': {
                    sub: {}
                }
            };
            this.saveToLocalstorage(localData);
        }
        else
            localData = JSON.parse(localData);
        return localData;
    };
    dataStore.prototype.saveToLocalstorage = function (data) {
        localStorage.setItem("demo", JSON.stringify(data));
    };
    dataStore.$inject = ['$rootScope'];
    return dataStore;
})();
services.service('dataStore', dataStore);
/// <reference path='jquery.d.ts' />
/// <reference path='angular.d.ts' />
/// <reference path='services/dataService.ts' /> 
/// <reference path='mainInterface.ts' />
/// <reference path='../modules.ts' />
var MainController = (function () {
    function MainController($scope, dataModel) {
        this.dataModel = dataModel;
        this.reversIndex = 0;
        $scope.vm = this;
        this.elements = dataModel.getFromLocalstorage();
    }
    MainController.prototype.getElements = function (name) {
        var target = this.elements[name];
        if (Object.keys(target).length > 0)
            return target;
        else
            return null;
    };
    MainController.prototype.incrementCounter = function (elm, obj) {
        var allElments = elm.split('_');
        var keyEl = allElments[0];
        if (this.reversIndex > 0)
            for (var a = 0; a <= this.reversIndex; a++)
                keyEl += allElments[a];
        var currentTarget = (obj) ? obj[elm] : this.elements[keyEl];
        var propCount = Object.keys(currentTarget['sub']).length;
        if (propCount > 0 && allElments[this.reversIndex + 1]) {
            this.reversIndex++;
            this.incrementCounter(elm, currentTarget['sub']);
        }
        else {
            this.reversIndex = 0;
            var childEl = { 'sub': {} };
            var keyString = (propCount == 0) ? (elm + '_1') : (elm + '_' + (propCount + 1));
            currentTarget['sub'][keyString] = childEl;
            this.dataModel.saveToLocalstorage(this.elements);
        }
    };
    MainController.$inject = ['$scope', 'dataStore'];
    return MainController;
})();
controllers.controller('MainController', MainController);
/// <reference path='refference.ts' />
/// <reference path='modules.ts' />
/// <reference path='controllers/mainController.ts' />
/// <reference path='controllers/mainInterface.ts' />
angular.module('demoApp', ['controllers', 'services']);
